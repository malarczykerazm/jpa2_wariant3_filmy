package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.ActorEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.enumerator.ColourType;
import com.capgemini.enumerator.Country;
import com.capgemini.enumerator.MovieGenre;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.NonSpecificSearchParametersException;
import com.capgemini.searchcriteria.MovieSearchCriteria;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class MovieServiceTest {

	@Autowired
	private MovieService movieService;

	@Autowired
	private StudioService studioService;

	@Autowired
	private ActorService actorService;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldFindAllMovies() {
		// given

		// when
		List<MovieEntity> allMovies = movieService.findAll();

		// then
		assertNotNull(allMovies);
		assertTrue(18 == allMovies.size());
	}

	@Test
	public void shouldFindMovieByID() {
		// given
		final Long id = 1L;

		// when
		MovieEntity movie = movieService.findByID(id);

		// then
		assertNotNull(movie);
		assertEquals("Fingers at the Window", movie.getTitle());
		assertEquals(MovieGenre.ROMANCE, movie.getGenre());
		assertEquals(ColourType.BLACK_AND_WHITE, movie.getColourType());
		assertTrue(50 == movie.getDuration());
		assertEquals(Date.valueOf("1970-04-30"), movie.getPremiere());
		assertEquals(true, movie.getIs3D());

	}

	@Test
	public void shouldSaveMovie() {
		// given
		MovieEntity movie = new MovieEntity();
		movie.setTitle("Team A");
		movie.setGenre(MovieGenre.MUSICAL);
		movie.setColourType(ColourType.COLOUR);
		movie.setDuration(90);
		movie.setPremiere(Date.valueOf("2000-01-01"));
		movie.setActors(new HashSet<ActorEntity>());
		movie.setCountries(new HashSet<Country>());
		movie.setStudios(new HashSet<StudioEntity>());
		movie.setIs3D(true);

		int sizeBefore = movieService.findAll().size();

		// when
		movieService.save(movie);
		List<MovieEntity> movies = movieService.findAll();

		// then
		assertEquals(movie.getTitle(), movies.get(movies.size() - 1).getTitle());
		assertEquals(movie.getDuration(), movies.get(movies.size() - 1).getDuration());
		assertEquals(movie.getPremiere(), movies.get(movies.size() - 1).getPremiere());
		assertTrue(sizeBefore + 1 == movies.size());
	}

	@Test
	public void shouldDeleteMovie() {
		// given
		final Long id = 1L;
		int sizeBefore = movieService.findAll().size();

		MovieEntity deletedMovie = movieService.findByID(id);
		List<StudioEntity> studiosWithDeletedMovie = studioService.findAll().stream()
				.filter(s -> s.getMovies().contains(deletedMovie)).collect(Collectors.toList());
		List<ActorEntity> actorsWithDeletedMovie = actorService.findAll().stream()
				.filter(a -> a.getMovies().contains(deletedMovie)).collect(Collectors.toList());

		// when
		movieService.delete(id);
		List<MovieEntity> moviesAfter = movieService.findAll();

		// then
		assertTrue(sizeBefore - 1 == moviesAfter.size());
		assertNotNull(studiosWithDeletedMovie);
		assertTrue(0 < studiosWithDeletedMovie.size());
		assertNull(studiosWithDeletedMovie.get(0).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(studiosWithDeletedMovie.get(1).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(0).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(1).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(2).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(3).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(4).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));
		assertNull(actorsWithDeletedMovie.get(5).getMovies().stream().filter(m -> m.equals(deletedMovie)).findAny()
				.orElse(null));

		// expect
		e.expect(NoSuchElementInDatabaseException.class);
		e.expectMessage("There was no");

		assertNull(movieService.findByID(id));
	}

	@Test
	public void shouldUpdateMovie() {
		// given
		final Long id = 1L;

		MovieEntity movie = movieService.findByID(id);

		final String newTitle = "Fight club";
		final String oldTitle = movie.getTitle();
		movie.setTitle(newTitle);

		int sizeBefore = movieService.findAll().size();

		// when
		movieService.update(movie);
		List<MovieEntity> movies = movieService.findAll();

		// then
		assertTrue(newTitle == movieService.findByID(id).getTitle());
		assertTrue(newTitle != oldTitle);
		assertTrue(sizeBefore == movies.size());
	}

	@Test
	public void shouldAssignActorToMovie() {
		// given
		final Long movieID = 1L;
		final Long actorID = 50L;

		MovieEntity movie = movieService.findByID(movieID);
		ActorEntity actor = actorService.findByID(actorID);

		int moviesSizeBefore = movieService.findAll().size();
		int actorsSizeBefore = actorService.findAll().size();

		// when
		movieService.assignActorToMovie(actor, movie);
		List<MovieEntity> movies = movieService.findAll();
		List<ActorEntity> actors = actorService.findAll();

		// then
		assertTrue(movieService.findByID(movieID).getActors().contains(actorService.findByID(actorID)));
		assertTrue(actorService.findByID(actorID).getMovies().contains(movieService.findByID(movieID)));
		assertTrue(moviesSizeBefore == movies.size());
		assertTrue(actorsSizeBefore == actors.size());
	}

	@Test
	public void shouldRemoveActorFromMovie() {
		// given
		final Long movieID = 1L;
		final Long actorID = 49L;

		MovieEntity movie = movieService.findByID(movieID);
		ActorEntity actor = actorService.findByID(actorID);

		int moviesSizeBefore = movieService.findAll().size();
		int actorsSizeBefore = actorService.findAll().size();

		// when
		movieService.removeActorFromMovie(actor, movie);
		List<MovieEntity> movies = movieService.findAll();
		List<ActorEntity> actors = actorService.findAll();

		// then
		assertFalse(movieService.findByID(movieID).getActors().contains(actorService.findByID(actorID)));
		assertFalse(actorService.findByID(actorID).getMovies().contains(movieService.findByID(movieID)));
		assertTrue(moviesSizeBefore == movies.size());
		assertTrue(actorsSizeBefore == actors.size());
	}

	@Test
	public void shouldFindMoviesByTitle() {
		// given
		final Long id = 1L;
		final String title = "Fingers";

		MovieEntity expectedMovie = movieService.findByID(id);

		// when
		List<MovieEntity> actualMovies = movieService.findByTitle(title);
		MovieEntity actualMovie = movieService.findByTitle(title).get(0);

		// then
		assertTrue(1 == actualMovies.size());
		assertEquals(expectedMovie, actualMovie);
	}

	@Test
	public void shouldFindMoviesByProvidedParameters() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setGenre(MovieGenre.ROMANCE);
		mSC.setColourType(ColourType.BLACK_AND_WHITE);
		mSC.setMinDurationInMinutes(50);
		mSC.setMaxDurationInMinutes(200);
		mSC.setPremiereSince(Date.valueOf("1965-1-1"));
		mSC.setPremiereTo(Date.valueOf("1970-4-31"));
		mSC.setStudio(studioService.findByID(1L));
		mSC.setIs3D(true);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedGenreOnly() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setGenre(MovieGenre.ROMANCE);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));
		expectedMovies.add(movieService.findByID(2L));
		expectedMovies.add(movieService.findByID(5L));
		expectedMovies.add(movieService.findByID(14L));
		expectedMovies.add(movieService.findByID(18L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedColourTypeOnly() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setColourType(ColourType.BLACK_AND_WHITE);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));
		expectedMovies.add(movieService.findByID(2L));
		expectedMovies.add(movieService.findByID(10L));
		expectedMovies.add(movieService.findByID(13L));
		expectedMovies.add(movieService.findByID(15L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedGenreAndColourType() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setGenre(MovieGenre.ROMANCE);
		mSC.setColourType(ColourType.BLACK_AND_WHITE);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));
		expectedMovies.add(movieService.findByID(2L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedParametersWithoutMaxDuration() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setGenre(MovieGenre.ROMANCE);
		mSC.setColourType(ColourType.BLACK_AND_WHITE);
		mSC.setMinDurationInMinutes(40);
		mSC.setPremiereSince(Date.valueOf("1965-1-1"));
		mSC.setPremiereTo(Date.valueOf("1970-4-31"));
		mSC.setStudio(studioService.findByID(1L));
		mSC.setIs3D(true);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedParametersWithoutMinDuration() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setGenre(MovieGenre.ROMANCE);
		mSC.setColourType(ColourType.BLACK_AND_WHITE);
		mSC.setMaxDurationInMinutes(200);
		mSC.setPremiereSince(Date.valueOf("1965-1-1"));
		mSC.setPremiereTo(Date.valueOf("1970-4-31"));
		mSC.setStudio(studioService.findByID(1L));
		mSC.setIs3D(true);

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedPremiereSinceOnly() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setPremiereSince(Date.valueOf("2000-1-1"));

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(4L));
		expectedMovies.add(movieService.findByID(8L));
		expectedMovies.add(movieService.findByID(9L));
		expectedMovies.add(movieService.findByID(10L));
		expectedMovies.add(movieService.findByID(13L));
		expectedMovies.add(movieService.findByID(14L));
		expectedMovies.add(movieService.findByID(17L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedPremiereToOnly() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setPremiereTo(Date.valueOf("1999-12-31"));

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(1L));
		expectedMovies.add(movieService.findByID(2L));
		expectedMovies.add(movieService.findByID(3L));
		expectedMovies.add(movieService.findByID(5L));
		expectedMovies.add(movieService.findByID(6L));
		expectedMovies.add(movieService.findByID(7L));
		expectedMovies.add(movieService.findByID(11L));
		expectedMovies.add(movieService.findByID(12L));
		expectedMovies.add(movieService.findByID(15L));
		expectedMovies.add(movieService.findByID(16L));
		expectedMovies.add(movieService.findByID(18L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindMoviesByProvidedStudioPremiereSinceAndPremiereTo() {
		// given
		MovieSearchCriteria mSC = new MovieSearchCriteria();
		mSC.setPremiereSince(Date.valueOf("1990-1-1"));
		mSC.setPremiereTo(Date.valueOf("1999-12-31"));
		mSC.setStudio(studioService.findByID(3L));

		List<MovieEntity> expectedMovies = new ArrayList<MovieEntity>();
		expectedMovies.add(movieService.findByID(3L));
		expectedMovies.add(movieService.findByID(16L));

		// when
		List<MovieEntity> actualMovies = movieService.findByProvidedParameters(mSC);

		// then
		assertEquals(expectedMovies, actualMovies);
	}

	@Test
	public void shouldFindLongestMovieMadeByStudio() {
		// given
		final Long id = 1L;

		String studioName = studioService.findByID(id).getStudioName();
		MovieEntity expectedMovie = movieService.findByID(12L);

		// when
		MovieEntity actualMovie = movieService.findLongestMovieMadeByStudio(studioName);

		// then
		assertEquals(expectedMovie, actualMovie);
	}

	@Test
	public void shouldThrowExceptionDueToMoreThanOneStudiosFound() throws NonSpecificSearchParametersException {
		// given
		String studioName = "Studio";

		// expect
		e.expect(NonSpecificSearchParametersException.class);
		e.expectMessage("There are more than one");

		// when
		movieService.findLongestMovieMadeByStudio(studioName);

		// then
		// EXCEPTION
	}

	@Test
	public void shouldThrowExceptionDueToNoStudiosFound() throws NonSpecificSearchParametersException {
		// given
		String studioName = "XYZ";

		// expect
		e.expect(NonSpecificSearchParametersException.class);
		e.expectMessage("There is no");

		// when
		movieService.findLongestMovieMadeByStudio(studioName);

		// then
		// EXCEPTION
	}

}
