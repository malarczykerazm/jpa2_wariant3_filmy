package com.capgemini.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddressServiceTest.class, StudioServiceTest.class, MovieServiceTest.class, ActorServiceTest.class,
		ServicesDatesAndOptimisticLockingTest.class })
public class ServiceTestSuite {

}
