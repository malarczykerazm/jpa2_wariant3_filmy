package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.enumerator.Country;
import com.capgemini.exception.NoSuchElementInDatabaseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AddressServiceTest {

	@Autowired
	private AddressService addressService;

	@Autowired
	private StudioService studioService;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldFindAllAddresses() {
		// given

		// when
		List<AddressEntity> allAddresses = addressService.findAll();

		// then
		assertNotNull(allAddresses);
		assertTrue(5 == allAddresses.size());
	}

	@Test
	public void shouldFindAddressById() {
		// given
		final Long id = 1L;

		// when
		AddressEntity address = addressService.findByID(id);

		// then
		assertNotNull(address);
		assertEquals("1", address.getAddressNumber());
		assertEquals("Kirkmanshulme Ln", address.getStreet());
		assertEquals("M12 4WB", address.getPostcode());
		assertEquals("Manchester", address.getCity());
		assertEquals(Country.UNITED_KINGDOM, address.getCountry());
	}

	@Test
	public void shouldSaveAddress() {
		// given
		AddressEntity address = new AddressEntity();
		address.setAddressNumber("1-2a");
		address.setStreet("ul. Nowogrodzka");
		address.setPostcode("53-000");
		address.setCity("Wrocław");
		address.setCountry(Country.POLAND);

		int sizeBefore = addressService.findAll().size();

		// when
		addressService.save(address);
		List<AddressEntity> addresses = addressService.findAll();

		// then
		assertEquals(address, addresses.get(addresses.size() - 1));
		assertTrue(sizeBefore + 1 == addresses.size());
	}

	@Test
	public void shouldDeleteAddress() {
		// given
		final Long id = 1L;
		int sizeBefore = addressService.findAll().size();

		AddressEntity deletedAddress = addressService.findByID(id);
		StudioEntity studioWithDeletedAddress = studioService.findAll().stream()
				.filter(s -> s.getAddress().equals(deletedAddress)).findFirst().orElse(null);

		// when
		addressService.delete(id);
		List<AddressEntity> addressesAfter = addressService.findAll();

		// then
		assertTrue(sizeBefore - 1 == addressesAfter.size());
		assertNotNull(studioWithDeletedAddress);
		assertNull(studioWithDeletedAddress.getAddress());

		// expect
		e.expect(NoSuchElementInDatabaseException.class);
		e.expectMessage("There was no");

		assertNull(addressService.findByID(id));
	}

	@Test
	public void shouldUpdateAddress() {
		// given
		final Long id = 1L;

		AddressEntity address = addressService.findByID(id);

		final String newStreet = "Długa";
		final String oldStreet = address.getStreet();
		address.setStreet(newStreet);

		int sizeBefore = addressService.findAll().size();

		// when
		addressService.update(address);
		List<AddressEntity> addresses = addressService.findAll();

		// then
		assertTrue(newStreet == addressService.findByID(id).getStreet());
		assertTrue(newStreet != oldStreet);
		assertTrue(sizeBefore == addresses.size());
	}

	@Test
	public void shouldFindAddressByNumber() {
		// given
		final String number = "1";

		List<AddressEntity> expectedAddresses = new ArrayList<AddressEntity>();
		expectedAddresses.add(addressService.findByID(1L));
		expectedAddresses.add(addressService.findByID(5L));

		// when
		List<AddressEntity> actualAddresses = addressService.findByNumber(number);

		// then
		assertEquals(expectedAddresses, actualAddresses);
	}

	@Test
	public void shouldFindAddressByStreet() {
		// given
		final String street = "Paderewskiego";

		List<AddressEntity> expectedAddresses = new ArrayList<AddressEntity>();
		expectedAddresses.add(addressService.findByID(2L));

		// when
		List<AddressEntity> actualAddresses = addressService.findByStreet(street);

		// then
		assertEquals(expectedAddresses, actualAddresses);
	}

	@Test
	public void shouldFindAddressByCity() {
		// given
		final String city = "Wrocław";

		List<AddressEntity> expectedAddresses = new ArrayList<AddressEntity>();
		expectedAddresses.add(addressService.findByID(2L));

		// when
		List<AddressEntity> actualAddresses = addressService.findByCity(city);

		// then
		assertEquals(expectedAddresses, actualAddresses);
	}

	@Test
	public void shouldFindAddressByPostcode() {
		// given
		final String postcode = "591";

		List<AddressEntity> expectedAddresses = new ArrayList<AddressEntity>();
		expectedAddresses.add(addressService.findByID(3L));

		// when
		List<AddressEntity> actualAddresses = addressService.findByPostcode(postcode);

		// then
		assertEquals(expectedAddresses, actualAddresses);
	}

	@Test
	public void shouldFindAddressByCountry() {
		// given
		final Country country = Country.UNITED_STATES;

		List<AddressEntity> expectedAddresses = new ArrayList<AddressEntity>();
		expectedAddresses.add(addressService.findByID(4L));

		// when
		List<AddressEntity> actualAddresses = addressService.findByCountry(country);

		// then
		assertEquals(expectedAddresses, actualAddresses);
	}
}
