package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AddressEntity;
import com.capgemini.enumerator.Country;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ServicesDatesAndOptimisticLockingTest {

	@Autowired
	private AddressService addressService;

	@PersistenceContext
	private EntityManager entityManager;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldThrowOptimisticLockException() throws ObjectOptimisticLockingFailureException {
		// given
		final long id = 1;

		AddressEntity address = addressService.findByID(id);
		AddressEntity anotherAddress = addressService.findByID(id);

		address.setAddressNumber("125a");
		entityManager.detach(address);

		anotherAddress.setStreet("XYZ");
		entityManager.detach(anotherAddress);

		addressService.update(address);
		entityManager.flush();

		// expect
		e.expect(ObjectOptimisticLockingFailureException.class);

		// when
		addressService.update(anotherAddress);

		// then
		// EXCEPTION
	}

	@Test
	public void shouldAddCreationDateToObject() throws ParseException {
		// given
		AddressEntity address = new AddressEntity();
		address.setAddressNumber("13");
		address.setStreet("Street");
		address.setPostcode("Postcode");
		address.setCity("City");
		address.setCountry(Country.POLAND);

		LocalDateTime currentLocalDateTime = LocalDateTime.now();

		// when
		AddressEntity addedAddress = addressService.save(address);

		// then
		assertNotNull(addedAddress.getCreationTime());
		assertEquals(currentLocalDateTime.getYear(),
				LocalDateTime.ofInstant(addedAddress.getCreationTime().toInstant(), ZoneId.systemDefault()).getYear());
		assertEquals(currentLocalDateTime.getMonth(),
				LocalDateTime.ofInstant(addedAddress.getCreationTime().toInstant(), ZoneId.systemDefault()).getMonth());
		assertEquals(currentLocalDateTime.getDayOfMonth(), LocalDateTime
				.ofInstant(addedAddress.getCreationTime().toInstant(), ZoneId.systemDefault()).getDayOfMonth());
		assertEquals(currentLocalDateTime.getHour(),
				LocalDateTime.ofInstant(addedAddress.getCreationTime().toInstant(), ZoneId.systemDefault()).getHour());

		assertNotNull(addedAddress.getModificationTime());
		assertEquals(currentLocalDateTime.getYear(), LocalDateTime
				.ofInstant(addedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getYear());
		assertEquals(currentLocalDateTime.getMonth(), LocalDateTime
				.ofInstant(addedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getMonth());
		assertEquals(currentLocalDateTime.getDayOfMonth(), LocalDateTime
				.ofInstant(addedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getDayOfMonth());
		assertEquals(currentLocalDateTime.getHour(), LocalDateTime
				.ofInstant(addedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getHour());
	}

	@Test
	public void shouldChangeModificationDateInObject() throws ParseException {
		// given
		final long id = 1L;
		AddressEntity address = addressService.findByID(id);
		entityManager.detach(address);

		LocalDateTime currentLocalDateTime = LocalDateTime.now();

		// when
		address.setStreet("Abc");
		addressService.update(address);
		entityManager.flush();
		AddressEntity updatedAddress = addressService.findByID(address.getID());

		// then
		assertNotNull(updatedAddress.getModificationTime());
		assertEquals(currentLocalDateTime.getYear(), LocalDateTime
				.ofInstant(updatedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getYear());
		assertEquals(currentLocalDateTime.getMonth(), LocalDateTime
				.ofInstant(updatedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getMonth());
		assertEquals(currentLocalDateTime.getDayOfMonth(), LocalDateTime
				.ofInstant(updatedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getDayOfMonth());
		assertEquals(currentLocalDateTime.getHour(), LocalDateTime
				.ofInstant(updatedAddress.getModificationTime().toInstant(), ZoneId.systemDefault()).getHour());
	}

}
