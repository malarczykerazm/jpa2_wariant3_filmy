package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.ActorEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.enumerator.Country;
import com.capgemini.exception.NoSuchElementInDatabaseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ActorServiceTest {

	@Autowired
	private ActorService actorService;

	@Autowired
	private MovieService movieService;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldFindAllActors() {
		// given

		// when
		List<ActorEntity> allActors = actorService.findAll();

		// then
		assertNotNull(allActors);
		assertTrue(50 == allActors.size());
	}

	@Test
	public void shouldFindActorByID() {
		// given
		final Long id = 1L;

		// when
		ActorEntity actor = actorService.findByID(id);

		// then
		assertNotNull(actor);
		assertEquals("Jesus", actor.getFirstname());
		assertEquals("Malinson", actor.getSurname());
		assertEquals(Date.valueOf("1975-10-19"), actor.getBirthDate());
		assertEquals(Country.PHILIPPINES, actor.getCountry());
	}

	@Test
	public void shouldSaveActor() {
		// given
		ActorEntity actor = new ActorEntity();
		actor.setFirstname("Jaromir");
		actor.setSurname("Nohavica");
		actor.setBirthDate(Date.valueOf("1948-10-10"));
		actor.setCountry(Country.CZECH_REPUBLIC);

		int sizeBefore = actorService.findAll().size();

		// when
		actorService.save(actor);
		List<ActorEntity> actors = actorService.findAll();

		// then
		assertEquals(actor.getFirstname(), actors.get(actors.size() - 1).getFirstname());
		assertEquals(actor.getSurname(), actors.get(actors.size() - 1).getSurname());
		assertEquals(actor.getBirthDate(), actors.get(actors.size() - 1).getBirthDate());
		assertEquals(actor.getCountry(), actors.get(actors.size() - 1).getCountry());
		assertTrue(sizeBefore + 1 == actors.size());
	}

	@Test
	public void shouldDeleteActor() {
		// given
		final Long id = 1L;
		int sizeBefore = actorService.findAll().size();

		ActorEntity deletedActor = actorService.findByID(id);
		List<MovieEntity> moviesWithDeletedActor = movieService.findAll().stream()
				.filter(m -> m.getActors().contains(deletedActor)).collect(Collectors.toList());

		// when
		actorService.delete(id);
		List<ActorEntity> actorsAfter = actorService.findAll();

		// then
		assertTrue(sizeBefore - 1 == actorsAfter.size());
		assertNotNull(moviesWithDeletedActor);
		assertTrue(0 < moviesWithDeletedActor.size());
		assertNull(moviesWithDeletedActor.get(0).getStudios().stream().filter(m -> m.equals(deletedActor)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedActor.get(1).getStudios().stream().filter(m -> m.equals(deletedActor)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedActor.get(2).getStudios().stream().filter(m -> m.equals(deletedActor)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedActor.get(3).getStudios().stream().filter(m -> m.equals(deletedActor)).findAny()
				.orElse(null));

		// expect
		e.expect(NoSuchElementInDatabaseException.class);
		e.expectMessage("There was no");

		assertNull(actorService.findByID(id));
	}

	@Test
	public void shouldUpdateActor() {
		// given
		final Long id = 1L;

		ActorEntity actor = actorService.findByID(id);

		final String newFirstname = "Artur";
		final String oldFirstname = actor.getFirstname();
		actor.setFirstname(newFirstname);

		int sizeBefore = actorService.findAll().size();

		// when
		actorService.update(actor);
		List<ActorEntity> actors = actorService.findAll();

		// then
		assertTrue(newFirstname == actorService.findByID(id).getFirstname());
		assertTrue(newFirstname != oldFirstname);
		assertTrue(sizeBefore == actors.size());
	}

	@Test
	public void shouldFindActorByFirstname() {
		// given
		final String firstname = "Isl";

		List<ActorEntity> expectedActors = new ArrayList<ActorEntity>();
		expectedActors.add(actorService.findByID(13L));

		// when
		List<ActorEntity> actualActors = actorService.findByFirstname(firstname);

		// then
		assertTrue(expectedActors.size() == actualActors.size());
		assertEquals(expectedActors, actualActors);
	}

	@Test
	public void shouldFindActorBySurname() {
		// given
		final String surname = "Dami";

		List<ActorEntity> expectedActors = new ArrayList<ActorEntity>();
		expectedActors.add(actorService.findByID(12L));

		// when
		List<ActorEntity> actualActors = actorService.findBySurname(surname);

		// then
		assertTrue(expectedActors.size() == actualActors.size());
		assertEquals(expectedActors, actualActors);
	}

	@Test
	public void shouldFindActorByFirstnameFirstLettersAndSurnameFirstLetters() {
		// given
		List<ActorEntity> expectedActors = new ArrayList<ActorEntity>();
		expectedActors.add(actorService.findByID(1L));
		expectedActors.add(actorService.findByID(10L));
		expectedActors.add(actorService.findByID(18L));
		expectedActors.add(actorService.findByID(21L));
		expectedActors.add(actorService.findByID(27L));
		expectedActors.add(actorService.findByID(43L));
		expectedActors.add(actorService.findByID(49L));

		List<Character> firstnameLetters = new ArrayList<Character>();
		firstnameLetters.add('J');

		List<Character> surnameLetters = new ArrayList<Character>();
		surnameLetters.add('K');
		surnameLetters.add('P');

		// when
		List<ActorEntity> actualActors = actorService.findByFirstnameFirstLetterOrSurnameFirstLetter(firstnameLetters,
				surnameLetters);
		// then
		assertTrue(expectedActors.size() == actualActors.size());
		assertEquals(expectedActors, actualActors);
	}

	@Test
	public void shouldFindActorWhoDidNotPlayInAnyMovieInPeriod() {
		// given
		final Date dateSince = Date.valueOf("1965-1-1");
		final Date dateTo = Date.valueOf("2016-1-1");

		List<ActorEntity> expectedActors = new ArrayList<ActorEntity>();
		expectedActors.add(actorService.findByID(3L));
		expectedActors.add(actorService.findByID(15L));
		expectedActors.add(actorService.findByID(17L));
		expectedActors.add(actorService.findByID(23L));
		expectedActors.add(actorService.findByID(30L));
		expectedActors.add(actorService.findByID(48L));

		// when
		List<ActorEntity> actualActors = actorService.findWhoDidNotPlayInAnyMovieInPeriod(dateSince, dateTo);
		// then
		assertTrue(expectedActors.size() == actualActors.size());
		assertEquals(expectedActors, actualActors);
	}

}
