package com.capgemini.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.enumerator.Country;
import com.capgemini.exception.NoSuchElementInDatabaseException;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudioServiceTest {

	@Autowired
	private StudioService studioService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private MovieService movieService;

	@Rule
	public ExpectedException e = ExpectedException.none();

	@Test
	public void shouldFindAllStudios() {
		// given

		// when
		List<StudioEntity> allStudios = studioService.findAll();

		// then
		assertNotNull(allStudios);
		assertTrue(5 == allStudios.size());
	}

	@Test
	public void shouldFindStudioByID() {
		// given
		final Long id = 1L;
		AddressEntity address = addressService.findByID(id);

		// when
		StudioEntity studio = studioService.findByID(id);

		// then
		assertNotNull(studio);
		assertEquals("Riverside Studios", studio.getStudioName());
		assertEquals(address, studio.getAddress());
	}

	@Test
	public void shouldSaveStudio() {
		// given
		AddressEntity address = new AddressEntity();
		address.setAddressNumber("1");
		address.setStreet("Valley");
		address.setPostcode("11-ABC");
		address.setCity("Berg");
		address.setCountry(Country.PAKISTAN);

		StudioEntity studio = new StudioEntity();
		studio.setStudioName("Films");
		studio.setAddress(address);

		int studiosSizeBefore = studioService.findAll().size();
		int addressesSizeBefore = addressService.findAll().size();

		// when
		studioService.save(studio);
		List<StudioEntity> studios = studioService.findAll();
		List<AddressEntity> addresses = addressService.findAll();

		// then
		assertEquals(studio.getStudioName(), studios.get(studios.size() - 1).getStudioName());
		assertEquals(address, studios.get(studios.size() - 1).getAddress());
		assertTrue(studiosSizeBefore + 1 == studios.size());
		assertTrue(addressesSizeBefore + 1 == addresses.size());
	}

	@Test
	public void shouldDeleteStudio() {
		// given
		final Long id = 1L;
		int sizeBefore = studioService.findAll().size();
		int sizeOfAddressesBefore = addressService.findAll().size();

		StudioEntity deletedStudio = studioService.findByID(id);
		List<MovieEntity> moviesWithDeletedStudio = movieService.findAll().stream()
				.filter(m -> m.getStudios().contains(deletedStudio)).collect(Collectors.toList());

		// when
		studioService.delete(id);
		List<StudioEntity> studiosAfter = studioService.findAll();

		// then
		assertTrue(sizeBefore - 1 == studiosAfter.size());
		assertNotNull(moviesWithDeletedStudio);
		assertTrue(0 < moviesWithDeletedStudio.size());
		assertNull(moviesWithDeletedStudio.get(0).getStudios().stream().filter(s -> s.equals(deletedStudio)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedStudio.get(1).getStudios().stream().filter(s -> s.equals(deletedStudio)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedStudio.get(2).getStudios().stream().filter(s -> s.equals(deletedStudio)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedStudio.get(3).getStudios().stream().filter(s -> s.equals(deletedStudio)).findAny()
				.orElse(null));
		assertNull(moviesWithDeletedStudio.get(4).getStudios().stream().filter(s -> s.equals(deletedStudio)).findAny()
				.orElse(null));
		assertTrue(sizeOfAddressesBefore - 1 == addressService.findAll().size());

		// expect
		e.expect(NoSuchElementInDatabaseException.class);
		e.expectMessage("There was no");

		assertNull(studioService.findByID(id));
	}

	@Test
	public void shouldUpdateStudio() {
		// given
		final Long id = 1L;

		StudioEntity studio = studioService.findByID(id);

		final String newName = "Długa";
		final String oldName = studio.getStudioName();
		studio.setStudioName(newName);

		int sizeBefore = studioService.findAll().size();

		// when
		studioService.update(studio);
		List<StudioEntity> studios = studioService.findAll();

		// then
		assertTrue(newName == studioService.findByID(id).getStudioName());
		assertTrue(newName != oldName);
		assertTrue(sizeBefore == studios.size());
	}

	@Test
	public void shouldFindStudioByName() {
		// given
		final String studioName = "WFF";

		List<StudioEntity> expectedStudios = new ArrayList<StudioEntity>();
		expectedStudios.add(studioService.findByID(2L));

		// when
		List<StudioEntity> actualStudios = studioService.findByName(studioName);

		// then
		assertTrue(expectedStudios.size() == actualStudios.size());
		assertEquals(expectedStudios, actualStudios);
	}

	@Test
	public void shouldFindStudioByAddress() {
		// given
		final Long id = 5L;
		AddressEntity address = addressService.findByID(id);
		StudioEntity expectedStudio = studioService.findByID(5L);

		// when
		StudioEntity actualStudio = studioService.findByAddress(address);

		// then
		assertEquals(expectedStudio, actualStudio);
	}

	@Test
	public void shouldCountTotalMoviesDurationInPeriod() throws ParseException {
		// given
		final Date dateSince = Date.valueOf("1990-1-1");
		final Date dateTo = Date.valueOf("1999-12-31");

		final Long id = 3L;
		StudioEntity studio = studioService.findByID(id);

		final long expectedDuration = 340;

		// when
		long actualDuration = studioService.countTotalMoviesDurationInPeriodForStudio(studio, dateSince, dateTo);

		// then
		assertEquals(expectedDuration, actualDuration);
	}

	@Test
	public void shouldCountMoviesInYearForAllStudios() throws ParseException {
		// given

		final int year = 2010;

		StudioEntity studio2 = studioService.findByID(2L);
		Long numberOfMovies2 = 2L;
		StudioEntity studio3 = studioService.findByID(3L);
		Long numberOfMovies3 = 1L;
		StudioEntity studio4 = studioService.findByID(4L);
		Long numberOfMovies4 = 1L;

		// when
		List<Tuple> actualResults = studioService.countMoviesInYearForStudio(year);

		// then

		assertTrue(3 == actualResults.size());
		assertEquals(studio2, actualResults.get(0).get(0));
		assertEquals(numberOfMovies2, actualResults.get(0).get(1));
		assertEquals(studio3, actualResults.get(1).get(0));
		assertEquals(numberOfMovies3, actualResults.get(1).get(1));
		assertEquals(studio4, actualResults.get(2).get(0));
		assertEquals(numberOfMovies4, actualResults.get(2).get(1));
	}

}
