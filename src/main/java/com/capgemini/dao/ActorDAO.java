package com.capgemini.dao;

import java.sql.Date;
import java.util.List;

import com.capgemini.domain.ActorEntity;

public interface ActorDAO extends DAO<ActorEntity, Long> {

	List<ActorEntity> findByFirstname(String firstname);

	List<ActorEntity> findBySurname(String surname);

	List<ActorEntity> findByFirstnameFirstLetterOrSurnameFirstLetter(List<Character> firstnameLetters,
			List<Character> surnameLetters);

	List<ActorEntity> findWhoDidNotPlayInAnyMovieInPeriod(Date dateSince, Date dateTo);

}
