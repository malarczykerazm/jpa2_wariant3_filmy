package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.AddressEntity;
import com.capgemini.enumerator.Country;

public interface AddressDAO extends DAO<AddressEntity, Long> {

	List<AddressEntity> findByNumber(String addressNumber);

	List<AddressEntity> findByStreet(String street);

	List<AddressEntity> findByCity(String city);

	List<AddressEntity> findByPostcode(String postcode);

	List<AddressEntity> findByCountry(Country country);
}
