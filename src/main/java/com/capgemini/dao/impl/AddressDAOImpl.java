package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.AddressDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.enumerator.Country;

@Repository
public class AddressDAOImpl extends AbstractDAO<AddressEntity, Long> implements AddressDAO {

	@Override
	public List<AddressEntity> findByNumber(String addressNumber) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery("addresses.findByNumber", AddressEntity.class);
		query.setParameter("address_number", addressNumber);
		return query.getResultList();
	}

	@Override
	public List<AddressEntity> findByStreet(String street) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery("addresses.findByStreet", AddressEntity.class);
		query.setParameter("street", street);
		return query.getResultList();
	}

	@Override
	public List<AddressEntity> findByCity(String city) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery("addresses.findByCity", AddressEntity.class);
		query.setParameter("city", city);
		return query.getResultList();
	}

	@Override
	public List<AddressEntity> findByPostcode(String postcode) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery("addresses.findByPostcode",
				AddressEntity.class);
		query.setParameter("postcode", postcode);
		return query.getResultList();
	}

	@Override
	public List<AddressEntity> findByCountry(Country country) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery("addresses.findByCountry",
				AddressEntity.class);
		query.setParameter("country", country.toString());
		return query.getResultList();
	}
}
