package com.capgemini.dao.impl;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.StudioDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.MovieEntity_;
import com.capgemini.domain.StudioEntity;
import com.capgemini.domain.StudioEntity_;

@Repository
public class StudioDAOImpl extends AbstractDAO<StudioEntity, Long> implements StudioDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<StudioEntity> findByName(String studioName) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<StudioEntity> criteriaQuery = criteriaBuilder.createQuery(StudioEntity.class);

		Root<StudioEntity> root = criteriaQuery.from(StudioEntity.class);

		Predicate studioNamePredicate = criteriaBuilder.like(root.get(StudioEntity_.studioName),
				"%" + studioName + "%");

		Predicate[] predicates = new Predicate[1];

		predicates[0] = studioNamePredicate;

		criteriaQuery.where(predicates);

		TypedQuery<StudioEntity> query = entityManager.createQuery(criteriaQuery);

		return query.getResultList();
	}

	@Override
	public StudioEntity findByAddress(AddressEntity address) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<StudioEntity> criteriaQuery = criteriaBuilder.createQuery(StudioEntity.class);

		Root<StudioEntity> root = criteriaQuery.from(StudioEntity.class);

		Predicate[] predicates = new Predicate[1];

		predicates[0] = criteriaBuilder.equal(root.get(StudioEntity_.address), address);

		criteriaQuery.where(predicates);

		TypedQuery<StudioEntity> query = entityManager.createQuery(criteriaQuery);

		return query.getSingleResult();
	}

	@Override
	public Long countTotalMoviesDurationInPeriodForStudio(StudioEntity studio, Date dateSince, Date dateTo) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

		Root<MovieEntity> root = criteriaQuery.from(MovieEntity.class);

		Predicate[] predicates = new Predicate[2];

		predicates[0] = criteriaBuilder.isMember(studio, root.get(MovieEntity_.studios));

		predicates[1] = criteriaBuilder.between(root.get(MovieEntity_.premiere), dateSince, dateTo);

		criteriaQuery.select(criteriaBuilder.sumAsLong(root.get(MovieEntity_.durationInMinutes))).where(predicates);

		TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);

		return query.getSingleResult().longValue();
	}

	@Override
	public List<Tuple> countMoviesInYearForAllStudios(int year) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();

		Root<StudioEntity> studioRoot = criteriaQuery.from(StudioEntity.class);
		Join<StudioEntity, MovieEntity> moviesToStudios = studioRoot.join(StudioEntity_.movies, JoinType.LEFT);

		Predicate[] predicates = new Predicate[1];

		predicates[0] = criteriaBuilder.between(moviesToStudios.get(MovieEntity_.premiere), Date.valueOf(year + "-1-1"),
				Date.valueOf(year + "-12-31"));

		criteriaQuery.multiselect(studioRoot, criteriaBuilder.count(moviesToStudios.get(MovieEntity_.id)))
				.where(predicates).groupBy(studioRoot.<Long> get(StudioEntity_.id));

		TypedQuery<Tuple> query = entityManager.createQuery(criteriaQuery);

		return query.getResultList();
	}

}
