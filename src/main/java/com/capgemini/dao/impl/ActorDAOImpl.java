package com.capgemini.dao.impl;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.ActorDAO;
import com.capgemini.domain.ActorEntity;
import com.capgemini.domain.QActorEntity;
import com.capgemini.domain.QMovieEntity;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class ActorDAOImpl extends AbstractDAO<ActorEntity, Long> implements ActorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<ActorEntity> findByFirstname(String firstname) {
		QActorEntity actor = QActorEntity.actorEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		List<ActorEntity> actors = query.select(actor).from(actor).where(actor.firstname.contains(firstname)).fetch();
		return actors;
	}

	@Override
	public List<ActorEntity> findBySurname(String surname) {
		QActorEntity actor = QActorEntity.actorEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		List<ActorEntity> actors = query.select(actor).from(actor).where(actor.surname.contains(surname)).fetch();
		return actors;
	}

	@Override
	public List<ActorEntity> findByFirstnameFirstLetterOrSurnameFirstLetter(List<Character> firstnameLetters,
			List<Character> surnameLetters) {
		QActorEntity actor = QActorEntity.actorEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		BooleanBuilder firstnamePredicateBuilder = new BooleanBuilder();
		BooleanBuilder surnamePredicateBuilder = new BooleanBuilder();

		for (Character letter : firstnameLetters) {
			firstnamePredicateBuilder.or(actor.firstname.startsWith(letter.toString()));
		}

		for (Character letter : surnameLetters) {
			surnamePredicateBuilder.or(actor.surname.startsWith(letter.toString()));
		}

		BooleanBuilder totalPredicateBuilder = new BooleanBuilder();
		totalPredicateBuilder.or(firstnamePredicateBuilder).or(surnamePredicateBuilder)
				.andNot(((firstnamePredicateBuilder).and(surnamePredicateBuilder)));

		List<ActorEntity> actors = query.select(actor).from(actor).where(totalPredicateBuilder.getValue()).fetch();
		return actors;
	}

	@Override
	public List<ActorEntity> findWhoDidNotPlayInAnyMovieInPeriod(Date dateSince, Date dateTo) {
		QActorEntity actor = QActorEntity.actorEntity;
		QMovieEntity movie = QMovieEntity.movieEntity;

		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		List<ActorEntity> actors = query.select(actor).from(actor).where(actor.notIn(query.selectDistinct(actor)
				.from(actor).join(actor.movies, movie).where(movie.premiere.between(dateSince, dateTo)))).fetch();
		return actors;
	}

}
