package com.capgemini.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.MovieDAO;
import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.MovieEntity_;
import com.capgemini.domain.StudioEntity;
import com.capgemini.searchcriteria.MovieSearchCriteria;

@Repository
public class MovieDAOImpl extends AbstractDAO<MovieEntity, Long> implements MovieDAO {

	@Override
	public List<MovieEntity> findByTitle(String title) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<MovieEntity> criteriaQuery = criteriaBuilder.createQuery(MovieEntity.class);

		Root<MovieEntity> root = criteriaQuery.from(MovieEntity.class);

		Predicate titlePredicate = criteriaBuilder.like(root.get(MovieEntity_.title), "%" + title + "%");

		Predicate[] predicates = new Predicate[1];

		predicates[0] = titlePredicate;

		criteriaQuery.where(predicates);

		TypedQuery<MovieEntity> query = entityManager.createQuery(criteriaQuery);

		return query.getResultList();
	}

	@Override
	public List<MovieEntity> findByProvidedParameters(MovieSearchCriteria mSC) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<MovieEntity> criteriaQuery = criteriaBuilder.createQuery(MovieEntity.class);

		Root<MovieEntity> root = criteriaQuery.from(MovieEntity.class);

		List<Predicate> listOfPredicates = new ArrayList<Predicate>();

		if (null != mSC.getGenre()) {
			listOfPredicates.add(criteriaBuilder.equal(root.get(MovieEntity_.genre), mSC.getGenre()));
		}

		if (null != mSC.getColourType()) {
			listOfPredicates.add(criteriaBuilder.equal(root.get(MovieEntity_.colourType), mSC.getColourType()));
		}

		if (null != mSC.getMinDurationInMinutes()) {
			listOfPredicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(MovieEntity_.durationInMinutes),
					mSC.getMinDurationInMinutes()));
		}

		if (null != mSC.getMaxDurationInMinutes()) {
			listOfPredicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(MovieEntity_.durationInMinutes),
					mSC.getMaxDurationInMinutes()));
		}

		if (null != mSC.getPremiereSince()) {
			listOfPredicates
					.add(criteriaBuilder.greaterThanOrEqualTo(root.get(MovieEntity_.premiere), mSC.getPremiereSince()));
		}

		if (null != mSC.getPremiereTo()) {
			listOfPredicates
					.add(criteriaBuilder.lessThanOrEqualTo(root.get(MovieEntity_.premiere), mSC.getPremiereTo()));
		}

		if (null != mSC.getStudio()) {
			listOfPredicates.add(criteriaBuilder.isMember(mSC.getStudio(), root.get(MovieEntity_.studios)));
		}

		if (null != mSC.getIs3D()) {
			listOfPredicates.add(criteriaBuilder.equal(root.get(MovieEntity_.is3D), mSC.getIs3D()));
		}

		Predicate[] predicates = listOfPredicates.toArray(new Predicate[listOfPredicates.size()]);

		criteriaQuery.where(predicates).orderBy(criteriaBuilder.asc(root.get(MovieEntity_.id)));

		TypedQuery<MovieEntity> query = entityManager.createQuery(criteriaQuery);

		return query.getResultList();
	}

	@Override
	public MovieEntity findLongestMovieMadeByStudio(StudioEntity studio) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<MovieEntity> criteriaQuery = criteriaBuilder.createQuery(MovieEntity.class);

		Root<MovieEntity> root = criteriaQuery.from(MovieEntity.class);

		Predicate[] predicates = new Predicate[1];

		predicates[0] = criteriaBuilder.isMember(studio, root.get(MovieEntity_.studios));

		criteriaQuery.where(predicates).orderBy(criteriaBuilder.desc(root.get(MovieEntity_.durationInMinutes)));

		TypedQuery<MovieEntity> query = entityManager.createQuery(criteriaQuery);

		return query.setMaxResults(1).getSingleResult();
	}

}
