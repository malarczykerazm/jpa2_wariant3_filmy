package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.searchcriteria.MovieSearchCriteria;

public interface MovieDAO extends DAO<MovieEntity, Long> {

	List<MovieEntity> findByTitle(String title);

	List<MovieEntity> findByProvidedParameters(MovieSearchCriteria mSC);

	MovieEntity findLongestMovieMadeByStudio(StudioEntity studio);

}
