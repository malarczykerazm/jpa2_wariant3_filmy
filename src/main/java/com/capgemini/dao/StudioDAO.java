package com.capgemini.dao;

import java.sql.Date;
import java.util.List;

import javax.persistence.Tuple;

import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.StudioEntity;

public interface StudioDAO extends DAO<StudioEntity, Long> {

	List<StudioEntity> findByName(String studioName);

	StudioEntity findByAddress(AddressEntity address);

	Long countTotalMoviesDurationInPeriodForStudio(StudioEntity studio, Date dateSince, Date dateTo);

	List<Tuple> countMoviesInYearForAllStudios(int year);
}
