package com.capgemini.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.enumerator.Country;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "addresses")
@NamedQueries({
		@NamedQuery(name = "addresses.findByNumber", query = "select a from AddressEntity a where a.addressNumber = :address_number"),
		@NamedQuery(name = "addresses.findByStreet", query = "select a from AddressEntity a where upper(a.street) like concat('%', upper(:street), '%')"),
		@NamedQuery(name = "addresses.findByCity", query = "select a from AddressEntity a where upper(a.city) like concat('%', upper(:city), '%')"),
		@NamedQuery(name = "addresses.findByPostcode", query = "select a from AddressEntity a where upper(a.postcode) like concat('%', upper(:postcode), '%')"),
		@NamedQuery(name = "addresses.findByCountry", query = "select a from AddressEntity a where upper(a.country) like concat('%', upper(:country), '%')") })
public class AddressEntity extends AbstractEntity {

	private static final long serialVersionUID = -309692233828238728L;

	@NotNull
	@Size(min = 1, max = 5)
	@Column(name = "address_number")
	private String addressNumber;

	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "street")
	private String street;

	@NotNull
	@Size(min = 1, max = 10)
	@Column(name = "postcode")
	private String postcode;

	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "city")
	private String city;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "country")
	private Country country;

	@OneToOne(mappedBy = "address", cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JsonIgnore
	private StudioEntity studio;

	@PreRemove
	private void setNullToAddressFieldInStudio() {
		studio.setAddress(null);
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@JsonIgnore
	public StudioEntity getStudio() {
		return studio;
	}

	@JsonIgnore
	public void setStudio(StudioEntity studio) {
		this.studio = studio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressNumber == null) ? 0 : addressNumber.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((postcode == null) ? 0 : postcode.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((studio == null) ? 0 : studio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressEntity other = (AddressEntity) obj;
		if (addressNumber == null) {
			if (other.addressNumber != null)
				return false;
		} else if (!addressNumber.equals(other.addressNumber))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country != other.country)
			return false;
		if (postcode == null) {
			if (other.postcode != null)
				return false;
		} else if (!postcode.equals(other.postcode))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (studio == null) {
			if (other.studio != null)
				return false;
		} else if (!studio.equals(other.studio))
			return false;
		return true;
	}

}
