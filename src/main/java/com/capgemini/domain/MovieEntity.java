package com.capgemini.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.capgemini.enumerator.ColourType;
import com.capgemini.enumerator.Country;
import com.capgemini.enumerator.MovieGenre;

@Entity
@Table(name = "movies")
public class MovieEntity extends AbstractEntity {

	private static final long serialVersionUID = 5378057831320272772L;

	@NotNull
	@Size(min = 1, max = 100)
	@Column(name = "title")
	private String title;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "genre")
	private MovieGenre genre;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "colour_type")
	private ColourType colourType;

	@NotNull
	@Range(min = 0, max = 999)
	@Column(name = "duration_in_minutes")
	private Integer durationInMinutes;

	@NotNull
	@Column(name = "premiere")
	private Date premiere;

	@NotNull
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JoinTable(name = "movies_to_actors", joinColumns = @JoinColumn(name = "movie_id") , inverseJoinColumns = @JoinColumn(name = "actor_id") )
	private Set<ActorEntity> actors;

	@NotNull
	@ElementCollection(targetClass = Country.class, fetch = FetchType.EAGER)
	@Enumerated(EnumType.STRING)
	@JoinTable(name = "movies_to_countries", joinColumns = @JoinColumn(name = "movie_id") )
	@Column(name = "country")
	private Set<Country> countries;

	@NotNull
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JoinTable(name = "movies_to_studios", joinColumns = @JoinColumn(name = "movie_id") , inverseJoinColumns = @JoinColumn(name = "studio_id") )
	private Set<StudioEntity> studios;

	@NotNull
	@Column(name = "is3d")
	private Boolean is3D;

	@PrePersist
	private void setDefaultMovieCountryFromStudioCountry() {
		Set<Country> countries = this.getCountries();
		if (studios.size() >= 1) {
			for (StudioEntity s : studios) {
				if (null != s.getAddress()) {
					countries.add(s.getAddress().getCountry());
				}
			}
		}
	}

	@PreRemove
	private void setNullWhereItsNeeded() {
		this.setNullInsteadOfThisMovieForActor();
		this.setNullInsteadOfThisMovieForStudio();
	}

	private void setNullInsteadOfThisMovieForActor() {
		for (ActorEntity a : actors) {
			Set<MovieEntity> moviesOfActor = a.getMovies();
			moviesOfActor.remove(this);
		}
	}

	private void setNullInsteadOfThisMovieForStudio() {
		for (StudioEntity s : studios) {
			Set<MovieEntity> moviesOfStudio = s.getMovies();
			moviesOfStudio.remove(this);
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieGenre getGenre() {
		return genre;
	}

	public void setGenre(MovieGenre genre) {
		this.genre = genre;
	}

	public ColourType getColourType() {
		return colourType;
	}

	public void setColourType(ColourType colourType) {
		this.colourType = colourType;
	}

	public Integer getDuration() {
		return durationInMinutes;
	}

	public void setDuration(Integer durationInMinutes) {
		this.durationInMinutes = durationInMinutes;
	}

	public Date getPremiere() {
		return premiere;
	}

	public void setPremiere(Date premiere) {
		this.premiere = premiere;
	}

	public Set<ActorEntity> getActors() {
		return actors;
	}

	public void setActors(Set<ActorEntity> actors) {
		this.actors = actors;
	}

	public Set<Country> getCountries() {
		return countries;
	}

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}

	public Set<StudioEntity> getStudios() {
		return studios;
	}

	public void setStudios(Set<StudioEntity> studios) {
		this.studios = studios;
	}

	public Boolean getIs3D() {
		return is3D;
	}

	public void setIs3D(Boolean is3D) {
		this.is3D = is3D;
	}

}
