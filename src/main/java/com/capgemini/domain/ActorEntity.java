package com.capgemini.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.enumerator.Country;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "actors")
public class ActorEntity extends AbstractEntity {

	private static final long serialVersionUID = -7769653744871301381L;

	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "firstname")
	private String firstname;

	@NotNull
	@Size(min = 1, max = 80)
	@Column(name = "surname")
	private String surname;

	@Column(name = "birth_date")
	private Date birthDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "country")
	private Country country;

	@ManyToMany(mappedBy = "actors", cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JsonIgnore
	private Set<MovieEntity> movies;

	@PreRemove
	private void setNullInsteadOfThisActorInMovies() {
		for (MovieEntity m : movies) {
			Set<ActorEntity> actorsOfMovie = m.getActors();
			actorsOfMovie.remove(this);
		}
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@JsonIgnore
	public Set<MovieEntity> getMovies() {
		return movies;
	}

	@JsonIgnore
	public void setMovies(Set<MovieEntity> movies) {
		this.movies = movies;
	}

}
