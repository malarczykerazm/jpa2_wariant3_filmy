package com.capgemini.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "studios")
public class StudioEntity extends AbstractEntity {

	private static final long serialVersionUID = -2975262168967508772L;

	@NotNull
	@Size(min = 1, max = 80)
	@Column(name = "studio_name", unique = true)
	private String studioName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", unique = true)
	private AddressEntity address;

	@ManyToMany(mappedBy = "studios", cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JsonIgnore
	private Set<MovieEntity> movies;

	@PreRemove
	private void setNullInsteadOfThisStudioInMovies() {
		for (MovieEntity m : movies) {
			Set<StudioEntity> studiosOfMovie = m.getStudios();
			studiosOfMovie.remove(this);
		}
	}

	public String getStudioName() {
		return studioName;
	}

	public void setStudioName(String studioName) {
		this.studioName = studioName;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	@JsonIgnore
	public Set<MovieEntity> getMovies() {
		return movies;
	}

	@JsonIgnore
	public void setMovies(Set<MovieEntity> movies) {
		this.movies = movies;
	}

}
