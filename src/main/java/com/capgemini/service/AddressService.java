package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.AddressEntity;
import com.capgemini.enumerator.Country;
import com.capgemini.exception.NoSuchElementInDatabaseException;

public interface AddressService {

	List<AddressEntity> findAll();

	AddressEntity findByID(Long id) throws NoSuchElementInDatabaseException;

	AddressEntity save(AddressEntity address);

	void delete(Long id) throws NoSuchElementInDatabaseException;

	AddressEntity delete(AddressEntity address) throws NoSuchElementInDatabaseException;

	AddressEntity update(AddressEntity address) throws NoSuchElementInDatabaseException;

	List<AddressEntity> findByNumber(String addressNumber) throws NoSuchElementInDatabaseException;

	List<AddressEntity> findByStreet(String street) throws NoSuchElementInDatabaseException;

	List<AddressEntity> findByCity(String city) throws NoSuchElementInDatabaseException;

	List<AddressEntity> findByPostcode(String postcode) throws NoSuchElementInDatabaseException;

	List<AddressEntity> findByCountry(Country country) throws NoSuchElementInDatabaseException;

}
