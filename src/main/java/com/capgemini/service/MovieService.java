package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.ActorEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.NonSpecificSearchParametersException;
import com.capgemini.searchcriteria.MovieSearchCriteria;

public interface MovieService {

	List<MovieEntity> findAll();

	MovieEntity findByID(Long id) throws NoSuchElementInDatabaseException;

	MovieEntity save(MovieEntity movie);

	void delete(Long id) throws NoSuchElementInDatabaseException;

	MovieEntity delete(MovieEntity movie) throws NoSuchElementInDatabaseException;

	MovieEntity update(MovieEntity movie) throws NoSuchElementInDatabaseException;

	MovieEntity assignActorToMovie(ActorEntity actor, MovieEntity movie) throws NoSuchElementInDatabaseException;

	MovieEntity removeActorFromMovie(ActorEntity actor, MovieEntity movie) throws NoSuchElementInDatabaseException;

	List<MovieEntity> findByTitle(String title) throws NoSuchElementInDatabaseException;

	List<MovieEntity> findByProvidedParameters(MovieSearchCriteria mSC) throws NoSuchElementInDatabaseException;

	MovieEntity findLongestMovieMadeByStudio(String studioName) throws NonSpecificSearchParametersException;

}
