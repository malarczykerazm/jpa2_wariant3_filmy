package com.capgemini.service;

import java.sql.Date;
import java.util.List;

import javax.persistence.Tuple;

import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;

public interface StudioService {

	List<StudioEntity> findAll();

	StudioEntity findByID(Long id) throws NoSuchElementInDatabaseException;

	StudioEntity save(StudioEntity studio);

	void delete(Long id) throws NoSuchElementInDatabaseException;

	StudioEntity delete(StudioEntity studio) throws NoSuchElementInDatabaseException;

	StudioEntity update(StudioEntity studio) throws NoSuchElementInDatabaseException;

	List<StudioEntity> findByName(String studioName) throws NoSuchElementInDatabaseException;

	StudioEntity findByAddress(AddressEntity address) throws NoSuchElementInDatabaseException;

	Long countTotalMoviesDurationInPeriodForStudio(StudioEntity studio, Date dateSince, Date dateTo)
			throws NoSuchElementInDatabaseException, NegativeTimeException;

	List<Tuple> countMoviesInYearForStudio(int year) throws NegativeTimeException;
}
