package com.capgemini.service;

import java.sql.Date;
import java.util.List;

import com.capgemini.domain.ActorEntity;
import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;

public interface ActorService {

	List<ActorEntity> findAll();

	ActorEntity findByID(Long id) throws NoSuchElementInDatabaseException;

	ActorEntity save(ActorEntity actor);

	void delete(Long id) throws NoSuchElementInDatabaseException;

	ActorEntity delete(ActorEntity actor) throws NoSuchElementInDatabaseException;

	ActorEntity update(ActorEntity actor) throws NoSuchElementInDatabaseException;

	List<ActorEntity> findByFirstname(String firstname) throws NoSuchElementInDatabaseException;

	List<ActorEntity> findBySurname(String surname) throws NoSuchElementInDatabaseException;

	List<ActorEntity> findByFirstnameFirstLetterOrSurnameFirstLetter(List<Character> firstnameLetters,
			List<Character> surnameLetters) throws NoSuchElementInDatabaseException;

	List<ActorEntity> findWhoDidNotPlayInAnyMovieInPeriod(Date dateSince, Date dateTo)
			throws NoSuchElementInDatabaseException, NegativeTimeException;

}
