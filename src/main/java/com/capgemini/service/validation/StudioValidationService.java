package com.capgemini.service.validation;

import java.util.List;

import com.capgemini.domain.StudioEntity;

public interface StudioValidationService {

	public void validateThereIsOneStudioOnTheList(List<StudioEntity> studiosByName, String studioName);

}
