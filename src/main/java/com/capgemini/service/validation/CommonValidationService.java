package com.capgemini.service.validation;

import java.sql.Date;
import java.util.List;

import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;

public interface CommonValidationService<T> {

	public void validateIfAnythingWasFound(T result, Long parameter) throws NoSuchElementInDatabaseException;

	public void validateIfAnythingWasFound(T result, String parameter) throws NoSuchElementInDatabaseException;

	public void validateIfAnythingWasFound(List<T> results, String parameter) throws NoSuchElementInDatabaseException;

	public void validateIfAnythingWasFound(List<T> results) throws NoSuchElementInDatabaseException;

	public void validateIfAnythingWasFound(T result) throws NoSuchElementInDatabaseException;

	public void validateIfAnythingWasFound(Class<T> clazz, Long parameter) throws NoSuchElementInDatabaseException;

	public void validateObjectInputForUpdate(T input) throws NoSuchElementInDatabaseException;

	public void validateObjectInputForDeleteOrSearch(T input) throws NoSuchElementInDatabaseException;

	public void validateDateSinceAndDateTo(Date dateSince, Date dateTo) throws NegativeTimeException;

}
