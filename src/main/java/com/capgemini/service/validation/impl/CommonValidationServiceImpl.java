package com.capgemini.service.validation.impl;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AbstractEntity;
import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.validation.CommonValidationService;

@Service
@Transactional(readOnly = true)
public class CommonValidationServiceImpl<T extends AbstractEntity> implements CommonValidationService<T> {

	@PersistenceContext
	private EntityManager entityManager;

	public void validateIfAnythingWasFound(T result, Long parameter) throws NoSuchElementInDatabaseException {
		if (null == result) {
			throw new NoSuchElementInDatabaseException(
					"There was no search results for the parameter: " + parameter + ".");
		}

	}

	@Override
	public void validateIfAnythingWasFound(T result, String parameter) throws NoSuchElementInDatabaseException {
		if (null == result) {
			throw new NoSuchElementInDatabaseException(
					"There was no search results for the parameter: " + parameter + ".");
		}

	}

	@Override
	public void validateIfAnythingWasFound(List<T> results, String parameter) throws NoSuchElementInDatabaseException {
		if (0 == results.size()) {
			throw new NoSuchElementInDatabaseException(
					"There was no search results for the parameter: " + parameter + ".");
		}

	}

	@Override
	public void validateIfAnythingWasFound(List<T> results) throws NoSuchElementInDatabaseException {
		if (0 == results.size()) {
			throw new NoSuchElementInDatabaseException("There was no search results for provided parameters.");
		}

	}

	@Override
	public void validateIfAnythingWasFound(T result) throws NoSuchElementInDatabaseException {
		if (null == result) {
			throw new NoSuchElementInDatabaseException("There was no search results for provided parameters.");
		}

	}

	@Override
	public void validateIfAnythingWasFound(Class<T> clazz, Long parameter) throws NoSuchElementInDatabaseException {
		if (null == entityManager.find(clazz, parameter)) {
			throw new NoSuchElementInDatabaseException(
					"There was no search results for the parameter: " + parameter + ".");
		}

	}

	@Override
	public void validateObjectInputForUpdate(T input) throws NoSuchElementInDatabaseException {
		if (null == input) {
			throw new NoSuchElementInDatabaseException("There was no valid input.");
		}
		if (null == input.getID()) {
			throw new NoSuchElementInDatabaseException("There was no valid input. The input has no ID.");
		}
		if (null == input.getID()) {
			throw new NoSuchElementInDatabaseException("There was no valid input. The input has no ID.");
		}
		if (null == entityManager.find(input.getClass(), input.getID())) {
			throw new NoSuchElementInDatabaseException(
					"There is no element in the database to process. Element ID not found.");
		}

	}

	@Override
	public void validateObjectInputForDeleteOrSearch(T input) throws NoSuchElementInDatabaseException {
		if (null == input) {
			throw new NoSuchElementInDatabaseException("There was no valid input.");
		}
		if (null == input.getID()) {
			throw new NoSuchElementInDatabaseException("There was no valid input. The input has no ID.");
		}
		if (null == input.getID()) {
			throw new NoSuchElementInDatabaseException("There was no valid input. The input has no ID.");
		}
		if (null == entityManager.find(input.getClass(), input.getID())) {
			throw new NoSuchElementInDatabaseException(
					"There is no element in the database to process. Element ID not found.");
		}
		if (!(input.equals(entityManager.find(input.getClass(), input.getID())))) {
			throw new NoSuchElementInDatabaseException(
					"There is no element in the database to process. Element ID was found, but the elements don't match.");
		}

	}

	@Override
	public void validateDateSinceAndDateTo(Date dateSince, Date dateTo) throws NegativeTimeException {
		if (dateSince.compareTo(dateTo) > 0) {
			throw new NegativeTimeException("The end date is before the start date.");
		}

	}

}
