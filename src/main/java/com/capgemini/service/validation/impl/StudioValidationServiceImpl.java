package com.capgemini.service.validation.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.StudioEntity;
import com.capgemini.exception.NonSpecificSearchParametersException;
import com.capgemini.service.validation.StudioValidationService;

@Service
@Transactional(readOnly = true)
public class StudioValidationServiceImpl implements StudioValidationService {

	@Override
	public void validateThereIsOneStudioOnTheList(List<StudioEntity> studiosByName, String studioName)
			throws NonSpecificSearchParametersException {

		if (1 < studiosByName.size()) {
			throw new NonSpecificSearchParametersException(
					"There are more than one film studio that contains " + studioName + "in its company name.");
		}

		if (0 == studiosByName.size()) {
			throw new NonSpecificSearchParametersException(
					"There is no film studio that contains " + studioName + " in its company name.");
		}
	}

}
