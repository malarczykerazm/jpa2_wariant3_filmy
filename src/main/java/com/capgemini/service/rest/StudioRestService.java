package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.StudioEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.StudioService;

@RestController
@RequestMapping(value = "/studios")
public class StudioRestService {

	@Autowired
	private StudioService studioService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<StudioEntity> findAll() {
		return studioService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public StudioEntity findByID(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return studioService.findByID(id);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public StudioEntity save(@RequestBody StudioEntity studio) {
		return studioService.save(studio);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public StudioEntity delete(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return studioService.delete(studioService.findByID(id));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public StudioEntity update(@RequestBody StudioEntity studio) throws NoSuchElementInDatabaseException {
		return studioService.update(studio);
	}

}
