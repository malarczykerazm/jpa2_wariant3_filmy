package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.ActorEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.ActorService;

@RestController
@RequestMapping(value = "/actors")
public class ActorRestService {

	@Autowired
	private ActorService actorService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<ActorEntity> findAll() {
		return actorService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ActorEntity findByID(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return actorService.findByID(id);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ActorEntity save(@RequestBody ActorEntity actor) {
		return actorService.save(actor);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public ActorEntity delete(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return actorService.delete(actorService.findByID(id));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ActorEntity update(@RequestBody ActorEntity actor) throws NoSuchElementInDatabaseException {
		return actorService.update(actor);
	}

}
