package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.AddressEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.AddressService;

@RestController
@RequestMapping(value = "/addresses")
public class AddressRestService {

	@Autowired
	private AddressService addressService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<AddressEntity> findAll() {
		return addressService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public AddressEntity findByID(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return addressService.findByID(id);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public AddressEntity save(@RequestBody AddressEntity address) {
		return addressService.save(address);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public AddressEntity delete(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return addressService.delete(addressService.findByID(id));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public AddressEntity update(@RequestBody AddressEntity address) throws NoSuchElementInDatabaseException {
		return addressService.update(address);
	}

}
