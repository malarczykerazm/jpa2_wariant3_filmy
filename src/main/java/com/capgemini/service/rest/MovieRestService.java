package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.domain.MovieEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.MovieService;

@RestController
@RequestMapping(value = "/movies")
public class MovieRestService {

	@Autowired
	private MovieService movieService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<MovieEntity> findAll() {
		return movieService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public MovieEntity findByID(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return movieService.findByID(id);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public MovieEntity save(@RequestBody MovieEntity movie) {
		return movieService.save(movie);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public MovieEntity delete(@PathVariable("id") long id) throws NoSuchElementInDatabaseException {
		return movieService.delete(movieService.findByID(id));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public MovieEntity update(@RequestBody MovieEntity movie) throws NoSuchElementInDatabaseException {
		return movieService.update(movie);
	}

}
