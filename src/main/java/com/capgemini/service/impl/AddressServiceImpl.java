package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.AddressDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.enumerator.Country;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.AddressService;
import com.capgemini.service.validation.CommonValidationService;

@Service
@Transactional(readOnly = true)
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDAO addressRepository;

	@Autowired
	private CommonValidationService<AddressEntity> commonValidation;

	@Override
	public List<AddressEntity> findAll() {
		return addressRepository.findAll();
	}

	@Override
	public AddressEntity findByID(Long id) throws NoSuchElementInDatabaseException {
		AddressEntity address = addressRepository.findOne(id);
		commonValidation.validateIfAnythingWasFound(address, id);
		return address;
	}

	@Override
	@Transactional(readOnly = false)
	public AddressEntity save(AddressEntity address) {
		return address = addressRepository.save(address);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Long id) throws NoSuchElementInDatabaseException {
		commonValidation.validateIfAnythingWasFound(AddressEntity.class, id);
		addressRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = false)
	public AddressEntity delete(AddressEntity address) throws NoSuchElementInDatabaseException {
		commonValidation.validateObjectInputForDeleteOrSearch(address);
		addressRepository.delete(address);
		return address;
	}

	@Override
	@Transactional(readOnly = false)
	public AddressEntity update(AddressEntity address) throws NoSuchElementInDatabaseException {
		commonValidation.validateObjectInputForUpdate(address);
		return addressRepository.update(address);
	}

	@Override
	public List<AddressEntity> findByNumber(String addressNumber) throws NoSuchElementInDatabaseException {
		List<AddressEntity> addresses = addressRepository.findByNumber(addressNumber);
		commonValidation.validateIfAnythingWasFound(addresses, addressNumber);
		return addresses;
	}

	@Override
	public List<AddressEntity> findByStreet(String street) throws NoSuchElementInDatabaseException {
		List<AddressEntity> addresses = addressRepository.findByStreet(street);
		commonValidation.validateIfAnythingWasFound(addresses, street);
		return addresses;
	}

	@Override
	public List<AddressEntity> findByCity(String city) throws NoSuchElementInDatabaseException {
		List<AddressEntity> addresses = addressRepository.findByCity(city);
		commonValidation.validateIfAnythingWasFound(addresses, city);
		return addresses;
	}

	@Override
	public List<AddressEntity> findByPostcode(String postcode) throws NoSuchElementInDatabaseException {
		List<AddressEntity> addresses = addressRepository.findByPostcode(postcode);
		commonValidation.validateIfAnythingWasFound(addresses, postcode);
		return addresses;
	}

	@Override
	public List<AddressEntity> findByCountry(Country country) throws NoSuchElementInDatabaseException {
		List<AddressEntity> addresses = addressRepository.findByCountry(country);
		commonValidation.validateIfAnythingWasFound(addresses, country.toString());
		return addresses;
	}

}
