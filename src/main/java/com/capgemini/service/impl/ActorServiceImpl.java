package com.capgemini.service.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ActorDAO;
import com.capgemini.domain.ActorEntity;
import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.ActorService;
import com.capgemini.service.validation.CommonValidationService;

@Service
@Transactional(readOnly = true)
public class ActorServiceImpl implements ActorService {

	@Autowired
	private ActorDAO actorRepository;

	@Autowired
	private CommonValidationService<ActorEntity> commonValidation;

	@Override
	public List<ActorEntity> findAll() {
		return actorRepository.findAll();
	}

	@Override
	public ActorEntity findByID(Long id) throws NoSuchElementInDatabaseException {
		ActorEntity actor = actorRepository.findOne(id);
		commonValidation.validateIfAnythingWasFound(actor, id);
		return actor;
	}

	@Override
	@Transactional(readOnly = false)
	public ActorEntity save(ActorEntity actor) {
		return actorRepository.save(actor);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Long id) throws NoSuchElementInDatabaseException {
		commonValidation.validateIfAnythingWasFound(ActorEntity.class, id);
		actorRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = false)
	public ActorEntity delete(ActorEntity actor) throws NoSuchElementInDatabaseException {
		commonValidation.validateObjectInputForDeleteOrSearch(actor);
		actorRepository.delete(actor);
		return actor;
	}

	@Override
	@Transactional(readOnly = false)
	public ActorEntity update(ActorEntity actor) throws NoSuchElementInDatabaseException {
		commonValidation.validateObjectInputForUpdate(actor);
		return actorRepository.update(actor);
	}

	@Override
	public List<ActorEntity> findByFirstname(String firstname) throws NoSuchElementInDatabaseException {
		List<ActorEntity> actors = actorRepository.findByFirstname(firstname);
		commonValidation.validateIfAnythingWasFound(actors, firstname);
		return actors;
	}

	@Override
	public List<ActorEntity> findBySurname(String surname) throws NoSuchElementInDatabaseException {
		List<ActorEntity> actors = actorRepository.findBySurname(surname);
		commonValidation.validateIfAnythingWasFound(actors, surname);
		return actors;
	}

	@Override
	public List<ActorEntity> findByFirstnameFirstLetterOrSurnameFirstLetter(List<Character> firstnameLetters,
			List<Character> surnameLetters) throws NoSuchElementInDatabaseException {
		List<ActorEntity> actors = actorRepository.findByFirstnameFirstLetterOrSurnameFirstLetter(firstnameLetters,
				surnameLetters);
		commonValidation.validateIfAnythingWasFound(actors);
		return actors;
	}

	@Override
	public List<ActorEntity> findWhoDidNotPlayInAnyMovieInPeriod(Date dateSince, Date dateTo)
			throws NoSuchElementInDatabaseException, NegativeTimeException {
		commonValidation.validateDateSinceAndDateTo(dateSince, dateTo);
		List<ActorEntity> actors = actorRepository.findWhoDidNotPlayInAnyMovieInPeriod(dateSince, dateTo);
		commonValidation.validateIfAnythingWasFound(actors);
		return actors;
	}

}