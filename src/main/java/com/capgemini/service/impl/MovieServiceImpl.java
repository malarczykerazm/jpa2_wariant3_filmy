package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.MovieDAO;
import com.capgemini.dao.StudioDAO;
import com.capgemini.domain.ActorEntity;
import com.capgemini.domain.MovieEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.NonSpecificSearchParametersException;
import com.capgemini.searchcriteria.MovieSearchCriteria;
import com.capgemini.service.MovieService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.service.validation.StudioValidationService;

@Service
@Transactional(readOnly = true)
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieDAO movieRepository;

	@Autowired
	private StudioDAO studioRepository;

	@Autowired
	private StudioValidationService studioValidation;

	@Autowired
	private CommonValidationService<MovieEntity> movieValidation;

	@Autowired
	private CommonValidationService<ActorEntity> actorValidation;

	@Override
	public List<MovieEntity> findAll() {
		return movieRepository.findAll();
	}

	@Override
	public MovieEntity findByID(Long id) throws NoSuchElementInDatabaseException {
		MovieEntity movie = movieRepository.findOne(id);
		movieValidation.validateIfAnythingWasFound(movie, id);
		return movie;
	}

	@Override
	@Transactional(readOnly = false)
	public MovieEntity save(MovieEntity movie) {
		return movieRepository.save(movie);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Long id) throws NoSuchElementInDatabaseException {
		movieValidation.validateIfAnythingWasFound(MovieEntity.class, id);
		movieRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = false)
	public MovieEntity delete(MovieEntity movie) throws NoSuchElementInDatabaseException {
		movieValidation.validateObjectInputForDeleteOrSearch(movie);
		movieRepository.delete(movie);
		return movie;
	}

	@Override
	@Transactional(readOnly = false)
	public MovieEntity update(MovieEntity movie) throws NoSuchElementInDatabaseException {
		movieValidation.validateObjectInputForUpdate(movie);
		return movieRepository.update(movie);
	}

	@Override
	public MovieEntity assignActorToMovie(ActorEntity actor, MovieEntity movie)
			throws NoSuchElementInDatabaseException {
		actorValidation.validateObjectInputForDeleteOrSearch(actor);
		movieValidation.validateObjectInputForDeleteOrSearch(movie);
		movie.getActors().add(actor);
		return movieRepository.update(movie);
	}

	@Override
	public MovieEntity removeActorFromMovie(ActorEntity actor, MovieEntity movie)
			throws NoSuchElementInDatabaseException {
		actorValidation.validateObjectInputForDeleteOrSearch(actor);
		movieValidation.validateObjectInputForDeleteOrSearch(movie);
		movie.getActors().remove(actor);
		return movieRepository.update(movie);
	}

	@Override
	public List<MovieEntity> findByTitle(String title) throws NoSuchElementInDatabaseException {
		List<MovieEntity> movies = movieRepository.findByTitle(title);
		movieValidation.validateIfAnythingWasFound(movies);
		return movies;
	}

	@Override
	public List<MovieEntity> findByProvidedParameters(MovieSearchCriteria mSC) throws NoSuchElementInDatabaseException {
		List<MovieEntity> movies = movieRepository.findByProvidedParameters(mSC);
		movieValidation.validateIfAnythingWasFound(movies);
		return movies;
	}

	@Override
	public MovieEntity findLongestMovieMadeByStudio(String studioName) throws NonSpecificSearchParametersException {
		List<StudioEntity> studiosByProvidedName = studioRepository.findByName(studioName);
		studioValidation.validateThereIsOneStudioOnTheList(studiosByProvidedName, studioName);
		return movieRepository.findLongestMovieMadeByStudio(studiosByProvidedName.get(0));
	}

}
