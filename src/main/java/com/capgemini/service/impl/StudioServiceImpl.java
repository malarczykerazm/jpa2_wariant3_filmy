package com.capgemini.service.impl;

import java.sql.Date;
import java.util.List;

import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.StudioDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.StudioEntity;
import com.capgemini.exception.NegativeTimeException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.StudioService;
import com.capgemini.service.validation.CommonValidationService;

@Service
@Transactional(readOnly = true)
public class StudioServiceImpl implements StudioService {

	@Autowired
	private StudioDAO studioRepository;

	@Autowired
	private CommonValidationService<StudioEntity> studioValidation;

	@Autowired
	private CommonValidationService<AddressEntity> addressValidation;

	@Override
	public List<StudioEntity> findAll() {
		return studioRepository.findAll();
	}

	@Override
	public StudioEntity findByID(Long id) throws NoSuchElementInDatabaseException {
		StudioEntity studio = studioRepository.findOne(id);
		studioValidation.validateIfAnythingWasFound(studio, id);
		return studio;
	}

	@Override
	@Transactional(readOnly = false)
	public StudioEntity save(StudioEntity studio) {
		return studioRepository.save(studio);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Long id) throws NoSuchElementInDatabaseException {
		studioValidation.validateIfAnythingWasFound(StudioEntity.class, id);
		studioRepository.delete(id);
	}

	@Override
	@Transactional(readOnly = false)
	public StudioEntity delete(StudioEntity studio) throws NoSuchElementInDatabaseException {
		studioValidation.validateObjectInputForDeleteOrSearch(studio);
		studioRepository.delete(studio);
		return studio;
	}

	@Override
	@Transactional(readOnly = false)
	public StudioEntity update(StudioEntity studio) throws NoSuchElementInDatabaseException {
		studioValidation.validateObjectInputForUpdate(studio);
		return studioRepository.update(studio);
	}

	@Override
	public List<StudioEntity> findByName(String studioName) throws NoSuchElementInDatabaseException {
		List<StudioEntity> studios = studioRepository.findByName(studioName);
		studioValidation.validateIfAnythingWasFound(studios, studioName);
		return studios;
	}

	@Override
	public StudioEntity findByAddress(AddressEntity address) throws NoSuchElementInDatabaseException {
		addressValidation.validateObjectInputForDeleteOrSearch(address);
		StudioEntity studio = studioRepository.findByAddress(address);
		studioValidation.validateIfAnythingWasFound(studio);
		return studio;
	}

	@Override
	public Long countTotalMoviesDurationInPeriodForStudio(StudioEntity studio, Date dateSince, Date dateTo)
			throws NoSuchElementInDatabaseException, NegativeTimeException {
		studioValidation.validateObjectInputForDeleteOrSearch(studio);
		studioValidation.validateDateSinceAndDateTo(dateSince, dateTo);
		return studioRepository.countTotalMoviesDurationInPeriodForStudio(studio, dateSince, dateTo);
	}

	@Override
	public List<Tuple> countMoviesInYearForStudio(int year) throws NegativeTimeException {
		if (year <= 0) {
			throw new NegativeArraySizeException("The year must be a positive number.");
		}
		return studioRepository.countMoviesInYearForAllStudios(year);
	}

}
