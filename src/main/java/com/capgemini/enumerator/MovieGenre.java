package com.capgemini.enumerator;

public enum MovieGenre {
	COMEDY, CRIME_STORY, HORROR, THRILLER, ROMANCE, MUSICAL, FAMILY, DRAMA, SCI_FI, FANTASY
}
