package com.capgemini.exception;

public class NonSpecificSearchParametersException extends RuntimeException {

	private static final long serialVersionUID = -6417079779586763677L;

	public NonSpecificSearchParametersException(String message) {
		super(message);
	}

}
