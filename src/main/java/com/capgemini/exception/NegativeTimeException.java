package com.capgemini.exception;

public class NegativeTimeException extends RuntimeException {

	private static final long serialVersionUID = -5711554031376555591L;

	public NegativeTimeException(String message) {
		super(message);
	}

}
