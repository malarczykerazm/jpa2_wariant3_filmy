package com.capgemini.searchcriteria;

import java.sql.Date;

import com.capgemini.domain.StudioEntity;
import com.capgemini.enumerator.ColourType;
import com.capgemini.enumerator.MovieGenre;

public class MovieSearchCriteria {

	private MovieGenre genre;

	private ColourType colourType;

	private Integer minDurationInMinutes;

	private Integer maxDurationInMinutes;

	private Date premiereSince;

	private Date premiereTo;

	private StudioEntity studio;

	private Boolean is3D;

	public MovieGenre getGenre() {
		return genre;
	}

	public void setGenre(MovieGenre genre) {
		this.genre = genre;
	}

	public ColourType getColourType() {
		return colourType;
	}

	public void setColourType(ColourType colourType) {
		this.colourType = colourType;
	}

	public Integer getMinDurationInMinutes() {
		return minDurationInMinutes;
	}

	public void setMinDurationInMinutes(Integer minDurationInMinutes) {
		this.minDurationInMinutes = minDurationInMinutes;
	}

	public Integer getMaxDurationInMinutes() {
		return maxDurationInMinutes;
	}

	public void setMaxDurationInMinutes(Integer maxDurationInMinutes) {
		this.maxDurationInMinutes = maxDurationInMinutes;
	}

	public Date getPremiereSince() {
		return premiereSince;
	}

	public void setPremiereSince(Date premiereSince) {
		this.premiereSince = premiereSince;
	}

	public Date getPremiereTo() {
		return premiereTo;
	}

	public void setPremiereTo(Date premiereTo) {
		this.premiereTo = premiereTo;
	}

	public StudioEntity getStudio() {
		return studio;
	}

	public void setStudio(StudioEntity studios) {
		this.studio = studios;
	}

	public Boolean getIs3D() {
		return is3D;
	}

	public void setIs3D(Boolean is3d) {
		this.is3D = is3d;
	}

}
